# Suggested project tools

## Clang-format

Use clang-format. For now, the default configuration will do.

## Pre-commit hook

The provided `pre-commit` file should be moved to or linked against
`.git/hooks/pre-commit`.

```sh
# Git executes hooks from inside the hooks' directory.
# As such, setup the link while inside the project's root directory:
ln --symbolic ../../project_scripts/pre-commit .git/hooks/pre-commit
```

* Use Git's default pre-commit hook to check for whitespace errors.
* Prevent the commit if running `clang-format-diff.py` on the diff would cause
changes.

The last point isn't perfect: if you run format on a file after staging it, but
don't stage the result of the format, the hook __will__ let the commit go
through.
