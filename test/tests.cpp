
// The tests included in this file are meant to stress test behavior of a
// general Bignum class representing traditional integers (as opposed to other
// types of groups in the number theoretical sense).
// For example, here we insist on characteristics like get_zero() < get_one()

#include <catch2/catch.hpp>

#include "test_support.h"

#include <random>

template <typename T> struct Fixture {};

// ========================= has_arbitrary_precision method available

// ========================= get_one/get_zero methods available
TEMPLATE_TEST_CASE_METHOD(Fixture, "one and zero", "[BignumGeneric]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const auto one = BignumTy::get_one();
  const auto one_other = BignumTy::get_one();
  REQUIRE(one.is_valid());
  REQUIRE(one_other.is_valid());

  const auto zero = BignumTy::get_zero();
  const auto zero_other = BignumTy::get_zero();
  REQUIRE(zero.is_valid());
  REQUIRE(zero_other.is_valid());
}

// ========================= comparison methods available
TEMPLATE_TEST_CASE_METHOD(Fixture, "comp one and zero", "[BignumGeneric]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const auto one = BignumTy::get_one();
  const auto one_other = BignumTy::get_one();
  REQUIRE(one == one_other);
  REQUIRE(one_other == one);
  REQUIRE(!(one < one_other));
  REQUIRE(!(one_other < one));

  const auto zero = BignumTy::get_zero();
  const auto zero_other = BignumTy::get_zero();
  REQUIRE(zero == zero_other);
  REQUIRE(!(zero < zero_other));
  REQUIRE(!(zero_other < zero));

  REQUIRE(zero < one);
  REQUIRE(!(one < zero));
}

// ========================= mix/max_element methods available
TEMPLATE_TEST_CASE_METHOD(Fixture, "min/max elements", "[BignumGeneric]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const auto min_element = BignumTy::min();
  REQUIRE(min_element.is_valid());
  const auto max_element = BignumTy::max();
  REQUIRE(max_element.is_valid());

  REQUIRE(min_element < max_element);
  REQUIRE(!(max_element < min_element));
  REQUIRE(!(max_element < max_element));
  REQUIRE(!(min_element < min_element));
  REQUIRE(!(min_element == max_element));
}

// ========================= from_hexadecimal_str methods available
TEMPLATE_TEST_CASE_METHOD(Fixture, "from_hexadecimal_str zero", "[BignumGeneric]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const auto zero = BignumTy::get_zero();

  auto test_lambda = [=](auto zero_str) {
    const auto zero_from_str = BignumTy::from_hexadecimal_str(zero_str);
    REQUIRE(zero_from_str.has_value());
    REQUIRE(zero_from_str.value().is_valid());
    REQUIRE(zero == zero_from_str);
  };

  // Use of sections here is questionable, as we are not modifying the contents
  // from the entry block. All we want is a way to print "test +0" or "test -0"
  // if the tests fail.
  SECTION("test +0") { test_lambda("+0"); }
  SECTION("test -0") { test_lambda("-0"); }
  SECTION("test non null terminated array") {
    const char non_null_term[] = {'+', '0'};
    test_lambda(std::string_view(non_null_term, 2));
    test_lambda(std::string_view(non_null_term + 1, 1));
  }
}

TEMPLATE_TEST_CASE_METHOD(Fixture, "from_hexadecimal_str one", "[BignumGeneric]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const auto one = BignumTy::get_one();
  const auto one_from_str = BignumTy::from_hexadecimal_str("+1");

  REQUIRE(one_from_str.has_value());
  REQUIRE(one_from_str.value().is_valid());
  REQUIRE(one == one_from_str);
}

TEMPLATE_TEST_CASE_METHOD(Fixture, "from_hexadecimal_str basic errors",
                          "[BignumGeneric]", IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  REQUIRE(!BignumTy::from_hexadecimal_str("").has_value());
  REQUIRE(!BignumTy::from_hexadecimal_str("+").has_value());
  REQUIRE(!BignumTy::from_hexadecimal_str("-").has_value());
  REQUIRE(!BignumTy::from_hexadecimal_str("0xa").has_value());
  REQUIRE(!BignumTy::from_hexadecimal_str(".").has_value());
  REQUIRE(!BignumTy::from_hexadecimal_str("g").has_value());
  REQUIRE(!BignumTy::from_hexadecimal_str("1.0").has_value());
  REQUIRE(!BignumTy::from_hexadecimal_str("0xb").has_value());
  REQUIRE(!BignumTy::from_hexadecimal_str(std::string_view()).has_value());
}

namespace {
/// Generate a vector with Bignum values in [0; upper_bound).
template <typename BignumTy>
std::vector<BignumTy> gen_0n_bignum_sequence(int upper_bound) {
  std::vector<BignumTy> vec;
  vec.reserve(static_cast<std::size_t>(upper_bound));
  std::generate_n(std::back_inserter(vec), upper_bound, [i = 0]() mutable {
    const std::string i_str = std::to_string(i);
    const auto opt_i_bignum = BignumTy::from_hexadecimal_str(i_str);
    REQUIRE(opt_i_bignum.has_value());
    i++;
    return opt_i_bignum.value();
  });
  return vec;
}
} // namespace

TEMPLATE_TEST_CASE_METHOD(Fixture, "from_hexadecimal_str comparisons",
                          "[BignumGeneric]", IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  constexpr auto upper_bound = 100;
  const auto vec = gen_0n_bignum_sequence<BignumTy>(upper_bound);

  for (size_t i = 0; i < upper_bound; i++) {
    for (size_t j = i + 1; j < upper_bound; j++) {
      REQUIRE(vec[i] < vec[j]);
      REQUIRE(!(vec[j] < vec[i]));
      REQUIRE(!(vec[j] == vec[i]));
      REQUIRE(!(vec[i] == vec[j]));
    }
  }
}

TEMPLATE_TEST_CASE_METHOD(Fixture, "from_hexadecimal_str_negative_numbers",
                          "BignumGeneric", IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const auto neg_one_from_int = BignumTy(-1);
  const auto neg_one_from_str = BignumTy::from_hexadecimal_str("-1").value();

  REQUIRE(neg_one_from_str == neg_one_from_int);
}

// ========================= to_hexadecimal_str methods available
TEMPLATE_TEST_CASE_METHOD(Fixture, "to_hexadecimal_str zero/one", "[BignumGeneric]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  constexpr auto buffer_size = 128;
  char buffer[buffer_size];

  {
    const auto one = BignumTy::get_one();
    const auto end = one.to_hexadecimal_str(buffer, buffer_size);
    REQUIRE(buffer != end);
  }
  {
    const auto one = BignumTy::get_one();
    const auto end = one.to_hexadecimal_str(buffer, buffer_size);
    REQUIRE(buffer != end);
  }
}

TEMPLATE_TEST_CASE_METHOD(Fixture, "to_hexadecimal_str basic errors",
                          "[BignumGeneric]", IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const auto one = BignumTy::get_one();
  constexpr auto buffer_size = 64;
  char buffer[buffer_size];

  REQUIRE(nullptr == one.to_hexadecimal_str(nullptr, 42));
  REQUIRE(buffer == one.to_hexadecimal_str(buffer, 0));
  REQUIRE(buffer == one.to_hexadecimal_str(buffer, -42));
}

// ========================= Mul method available.
TEMPLATE_TEST_CASE_METHOD(Fixture, "mul_zero_one", "[BignumGeneric]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const auto one = BignumTy::get_one();
  const auto zero = BignumTy::get_zero();

  const auto one_times_one = one.mul(one);
  REQUIRE(one_times_one.is_valid());
  REQUIRE(one_times_one == one);

  const auto zero_times_one = one.mul(zero);
  REQUIRE(zero_times_one.is_valid());
  REQUIRE(zero_times_one == zero);

  const auto one_times_zero = zero.mul(one);
  REQUIRE(one_times_zero.is_valid());
  REQUIRE(one_times_zero == zero);
}

TEMPLATE_TEST_CASE_METHOD(Fixture, "constexpr mul_zero_one", "[BignumGeneric]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  constexpr auto one = BignumTy::get_one();
  constexpr auto zero = BignumTy::get_zero();

  constexpr auto one_times_one = one.mul(one);
  static_assert(one_times_one.is_valid());
  static_assert(one_times_one == one);

  constexpr auto zero_times_one = one.mul(zero);
  static_assert(zero_times_one.is_valid());
  static_assert(zero_times_one == zero);

  constexpr auto one_times_zero = zero.mul(one);
  static_assert(one_times_zero.is_valid());
  static_assert(one_times_zero == zero);
}

TEMPLATE_TEST_CASE_METHOD(Fixture, "mul_zero_one_arbitrary_int",
                          "[BignumGeneric]", IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const auto one = BignumTy::get_one();
  const auto zero = BignumTy::get_zero();

  constexpr auto upper_bound = 100;
  for (int i = 0; i < upper_bound; i++) {
    const auto num_str = std::to_string(i);
    const auto optional_num = BignumTy::from_hexadecimal_str(num_str);
    REQUIRE(optional_num.has_value());
    const auto num = optional_num.value();

    const auto num_times_one = num.mul(one);
    REQUIRE(num_times_one.is_valid());
    const auto one_times_num = one.mul(num);
    REQUIRE(one_times_num.is_valid());
    REQUIRE(num_times_one == num);
    REQUIRE(one_times_num == num);
    REQUIRE(one_times_num == num_times_one);

    const auto num_times_zero = num.mul(zero);
    const auto zero_times_num = zero.mul(num);
    REQUIRE(num_times_zero == zero);
    REQUIRE(zero_times_num == zero);
    REQUIRE(zero_times_num == num_times_zero);
  }
}

// Generates two random 32 bit signed integers, if their product fits a 32
// bit integer, performs the same operation on BignumTy and compares the
// results.
TEMPLATE_TEST_CASE_METHOD(Fixture, "mul_random", "[BignumGeneric]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;
  using ValueTy = int32_t;
  std::mt19937 rng(42);

  // Divide the limits by 2^16 so that the product never overflow / underflows.
  constexpr auto num_bits = static_cast<ValueTy>(8 * sizeof(ValueTy));
  constexpr auto bound_adjust = 1 << (num_bits/2);
  constexpr ValueTy lower_bound = std::numeric_limits<ValueTy>::min() / bound_adjust;
  constexpr ValueTy upper_bound = std::numeric_limits<ValueTy>::max() / bound_adjust;
  auto uniform =
      std::uniform_int_distribution<ValueTy>(lower_bound, upper_bound);

  // c++20: ranges
  constexpr auto num_iterations = 1e6;
  for (int it = 0; it < num_iterations; ++it) {
    const ValueTy v1 = uniform(rng);
    const ValueTy v2 = uniform(rng);
    const ValueTy product = v1 * v2;

    const BignumTy big_v1(v1);
    const BignumTy big_v2(v2);
    REQUIRE(big_v1.is_valid());
    REQUIRE(big_v2.is_valid());
    const BignumTy big_product = big_v1.mul(big_v2);
    const BignumTy big_expected(product);

    REQUIRE(big_product.is_valid());
    REQUIRE(big_product == big_expected);
  }
}

TEMPLATE_TEST_CASE_METHOD(Fixture, "mul_overflow", "[BignumGeneric]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  {
    const auto smallest = BignumTy::min();
    const auto largest = BignumTy::max();
    const auto zero = BignumTy::get_zero();
    const auto two = BignumTy::get_one().add(BignumTy::get_one());

    REQUIRE(smallest.mul(two).is_valid() == false);
    REQUIRE(largest.mul(two).is_valid() == false);
    REQUIRE(smallest.mul(zero).is_valid());
    REQUIRE(largest.mul(zero).is_valid());
  }
  {
    constexpr auto smallest = BignumTy::min();
    constexpr auto largest = BignumTy::max();
    constexpr auto zero = BignumTy::get_zero();
    constexpr auto two = BignumTy::get_one().add(BignumTy::get_one());

    static_assert(smallest.mul(two).is_valid() == false);
    static_assert(largest.mul(two).is_valid() == false);
    static_assert(smallest.mul(zero).is_valid());
    static_assert(largest.mul(zero).is_valid());
  }
}

// ========================= Sum method available
TEMPLATE_TEST_CASE_METHOD(Fixture, "add_zero_one", "[BignumGeneric]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const auto zero = BignumTy::get_zero();
  REQUIRE(zero.add(zero) == zero);

  const auto one = BignumTy::get_one();
  REQUIRE(zero.add(one) == one);
  REQUIRE(one.add(zero) == one);
}

TEMPLATE_TEST_CASE_METHOD(Fixture, "constexpr add_zero_one", "[BignumGeneric]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  constexpr auto zero = BignumTy::get_zero();
  static_assert(zero.add(zero) == zero);

  constexpr auto one = BignumTy::get_one();
  static_assert(zero.add(one) == one);
  static_assert(one.add(zero) == one);
}

TEMPLATE_TEST_CASE_METHOD(Fixture, "add_zero_one_arbitrary_int",
                          "[BignumGeneric]", IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const auto one = BignumTy::get_one();
  const auto zero = BignumTy::get_zero();

  constexpr auto upper_bound = 100;
  for (int i = 0; i < upper_bound; i++) {
    const auto num = BignumTy(i);
    REQUIRE(num.is_valid());
    const auto ans = BignumTy(i + 1);
    REQUIRE(ans.is_valid());

    const auto num_plus_one = num.add(one);
    REQUIRE(num_plus_one.is_valid());
    const auto one_plus_num = one.add(num);
    REQUIRE(one_plus_num.is_valid());
    REQUIRE(num_plus_one == ans);
    REQUIRE(one_plus_num == ans);
    REQUIRE(one_plus_num == num_plus_one);

    const auto num_plus_zero = num.add(zero);
    const auto zero_plus_num = zero.add(num);
    REQUIRE(num_plus_zero == num);
    REQUIRE(zero_plus_num == num);
    REQUIRE(zero_plus_num == num_plus_zero);
  }
}

// Generates two random 32 bit signed integers, if their sum fits a 32 bit
// integer, performs the same operation on BignumTy and compares the results.
TEMPLATE_TEST_CASE_METHOD(Fixture, "add_random", "[BignumGeneric]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;
  using ValueTy = int32_t;
  std::mt19937 rng(42);

  // Divide the limits by 2 so that the sum never overflow / underflows.
  constexpr ValueTy lower_bound = std::numeric_limits<ValueTy>::min() / 2;
  constexpr ValueTy upper_bound = std::numeric_limits<ValueTy>::max() / 2;
  auto uniform =
      std::uniform_int_distribution<ValueTy>(lower_bound, upper_bound);

  // c++20: ranges
  constexpr auto num_iterations = 1e6;
  for (int it = 0; it < num_iterations; ++it) {
    const ValueTy v1 = uniform(rng);
    const ValueTy v2 = uniform(rng);
    const ValueTy sum = v1 + v2;

    const BignumTy big_v1(v1);
    const BignumTy big_v2(v2);
    REQUIRE(big_v1.is_valid());
    REQUIRE(big_v2.is_valid());
    const BignumTy big_sum = big_v1.add(big_v2);
    const BignumTy big_expected(sum);
    REQUIRE(big_sum.is_valid());
    REQUIRE(big_sum == big_expected);
  }
}

TEMPLATE_TEST_CASE_METHOD(Fixture, "subtract_zero_one_arbitrary_int",
                          "[BignumGeneric]", IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const auto minus_one = BignumTy(-1);
  const auto zero = BignumTy::get_zero();

  // c++20: ranges.
  constexpr auto upper_bound = 100;
  constexpr auto lower_bound = -100;
  for (int i = lower_bound; i < upper_bound; i++) {
    const auto num = BignumTy(i);
    const auto expected_num_minus_one = BignumTy(i - 1);

    const auto num_minus_one = num.add(minus_one);
    REQUIRE(num_minus_one.is_valid());
    REQUIRE(num_minus_one == expected_num_minus_one);

    const auto minus_one_plus_num = minus_one.add(num);
    REQUIRE(minus_one_plus_num.is_valid());
    REQUIRE(minus_one_plus_num == expected_num_minus_one);

    const auto num_minus_zero = num.add(zero);
    REQUIRE(num_minus_zero == num);
  }
}

TEMPLATE_TEST_CASE_METHOD(Fixture, "compute largest factorial",
                          "[BignumGeneric]", IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  // Start with 1, 2;
  auto factorial = BignumTy::get_one();
  auto new_factorial = factorial.add(factorial);
  int factorial_it = 1;
  constexpr int MAX_IT = 100;

  // Invariant: factorial is valid and is equal to `factorial_it!`.
  while (new_factorial.is_valid() && factorial_it < MAX_IT) {
    REQUIRE(factorial < new_factorial);
    factorial_it++;
    factorial = new_factorial;
    new_factorial = factorial.mul(BignumTy(factorial_it + 1));
  }

  if constexpr (BignumTy::has_arbitrary_precision() == false)
    REQUIRE(factorial < BignumTy::max());
  else
    REQUIRE(factorial_it == MAX_IT);

  fmt::print("Maximum computed factorial is {}! = {}\n", factorial_it,
             factorial);
}
