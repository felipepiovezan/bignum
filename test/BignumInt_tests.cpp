// These tests are meant for BignumInt-specific behavior.

#include <catch2/catch.hpp>

#include "bignum_int/bignum_int.h"

#include <limits>

using namespace bignum;

// ========================= has_arbitrary_precision method available
TEST_CASE("check_precision_BignumInt", "[BignumInt]") {
  REQUIRE(BignumInt::has_arbitrary_precision() == false);
  static_assert(BignumInt::has_arbitrary_precision() == false);
}

// ========================= comparison methods available
// ========================= mix/max_element methods available
TEST_CASE("min/max elements_BignumInt", "[BignumInt]") {
  const auto min_element = BignumInt::min();
  const auto max_element = BignumInt::max();
  const auto expected_min = BignumInt(std::numeric_limits<int32_t>::min());
  const auto expected_max = BignumInt(std::numeric_limits<int32_t>::max());

  REQUIRE(min_element == expected_min);
  REQUIRE(max_element == expected_max);
}

// ========================= format methods avaiable
TEST_CASE("format BignumInt", "[BignumInt]") {
  using ValueTy = int32_t;
  const auto min_element = BignumInt::min();
  const auto min_element_str = fmt::format("{}", min_element);
  const auto expected_min_str = fmt::format(
      "valid = 1 value = {:0>32b}", std::numeric_limits<ValueTy>::min());
  REQUIRE(min_element_str == expected_min_str);
  REQUIRE(min_element == BignumInt(std::numeric_limits<ValueTy>::min()));

  const auto max_element = BignumInt::max();
  const auto max_element_str = fmt::format("{}", max_element);
  const auto expected_max_str = fmt::format(
      "valid = 1 value = {:0>32b}", std::numeric_limits<ValueTy>::max());
  REQUIRE(max_element_str == expected_max_str);
  REQUIRE(max_element == BignumInt(std::numeric_limits<ValueTy>::max()));

  constexpr auto upper_bound = 10000;
  constexpr auto lower_bound = -10000;
  // c++20: ranges
  for (int i = lower_bound; i < upper_bound; i++) {
    const auto element_str = fmt::format("{}", BignumInt(i));
    const auto expected_str = fmt::format("valid = 1 value = {:0>32b}", i);
    REQUIRE(element_str == expected_str);
  }
}
