#include <catch2/catch.hpp>

#include "bignum_array/bignum_array.h"

#include <random>

template <typename T> struct Fixture {};

#define IMPLEMENTATIONS_TO_TEST (bignum::BignumArray<4>)

// ========================= has_arbitrary_precision method available
TEMPLATE_TEST_CASE_METHOD(Fixture, "bignum_array_mul_random_numbers",
                          "[BignumArray]", IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;
  using ValueTy = int32_t;
  using QuadValueTy = __int128_t;
  std::mt19937 rng(42);

  constexpr ValueTy lower_bound = std::numeric_limits<ValueTy>::min();
  constexpr ValueTy upper_bound = std::numeric_limits<ValueTy>::max();
  auto uniform =
      std::uniform_int_distribution<ValueTy>(lower_bound, upper_bound);

  // c++20: ranges
  constexpr auto num_iterations = 1e6;
  for (int it = 0; it < num_iterations; ++it) {
    QuadValueTy v1 = uniform(rng);
    QuadValueTy v2 = uniform(rng);
    QuadValueTy v3 = uniform(rng);
    QuadValueTy v4 = uniform(rng);
    QuadValueTy product12 = v1 * v2;
    QuadValueTy product34 = v3 * v4;
    QuadValueTy product1234 = product12 * product34;

    BignumTy big_v1(v1);
    BignumTy big_v2(v2);
    BignumTy big_v3(v3);
    BignumTy big_v4(v4);
    REQUIRE(big_v1.is_valid());
    REQUIRE(big_v2.is_valid());
    REQUIRE(big_v3.is_valid());
    REQUIRE(big_v4.is_valid());

    BignumTy big_product12 = big_v1.mul(big_v2);
    REQUIRE(big_product12.is_valid());
    BignumTy expected_big_product12(product12);
    REQUIRE(expected_big_product12.is_valid());
    REQUIRE(expected_big_product12 == big_product12);

    BignumTy big_product34 = big_v3.mul(big_v4);
    REQUIRE(big_product34.is_valid());
    BignumTy expected_big_product34(product34);
    REQUIRE(expected_big_product34.is_valid());
    REQUIRE(expected_big_product34 == big_product34);

    BignumTy big_product1234 = big_product12.mul(big_product34);
    REQUIRE(big_product1234.is_valid());
    BignumTy expected_big_product1234(product1234);
    REQUIRE(expected_big_product1234.is_valid());
    REQUIRE(expected_big_product1234 == big_product1234);
  }
}

// ========================= format methods avaiable
TEMPLATE_TEST_CASE_METHOD(Fixture, "BignumArray min_max_from_int", "[BignumArray]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;
  using ValueTy = __int128_t;

  auto min_from_int = BignumTy(std::numeric_limits<ValueTy>::min());
  REQUIRE(min_from_int.is_valid());

  auto almost_min_from_int = BignumTy(1 + std::numeric_limits<ValueTy>::min());
  REQUIRE(almost_min_from_int.is_valid());

  auto back_to_min = almost_min_from_int.add(BignumTy(-1));
  REQUIRE(back_to_min.is_valid());
  REQUIRE(back_to_min == min_from_int);

  auto max_from_int = BignumTy(std::numeric_limits<ValueTy>::max());
  REQUIRE(max_from_int.is_valid());

  auto almost_max_from_int = BignumTy(-1 + std::numeric_limits<ValueTy>::max());
  REQUIRE(almost_max_from_int.is_valid());

  auto back_to_max = almost_max_from_int.add(BignumTy::get_one());
  REQUIRE(back_to_max.is_valid());
  REQUIRE(back_to_max == max_from_int);
}
