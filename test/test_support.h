#pragma once

// To register an implementation of the Bignum interface for testing,
// include its header file here and add it to the list in
// IMPLEMENTATIONS_TO_TEST.

#include "bignum_int/bignum_int.h"
#include "bignum_array/bignum_array.h"
#include <fmt/format.h>

#define IMPLEMENTATIONS_TO_TEST (bignum::BignumInt), (bignum::BignumArray<4>)
