
// The tests included in this file are meant to stress the *interface* of a
// Bignum class, not the semantics of the returned values.

// As such, it should be able to use these tests for non-conventional
// groups/rings in the number theoretical sense. This is the intended direction
// for this test and, if explored, the from/to string methods may not make
// sense and should be revisited.

#include <catch2/catch.hpp>

#include "test_support.h"

template <typename T> struct Fixture {};

// ========================= has_arbitrary_precision method available
TEMPLATE_TEST_CASE_METHOD(Fixture, "has_arbitrary_precision_interface",
                          "[BignumInterface]", IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const bool call_ok = BignumTy::has_arbitrary_precision() || true;
  REQUIRE(call_ok);
  constexpr bool constexpr_call_ok =
      BignumTy::has_arbitrary_precision() || true;
  static_assert(constexpr_call_ok);
}

// ========================= is_valid/get_one/get_zero methods available
TEMPLATE_TEST_CASE_METHOD(Fixture, "is_valid/get_one_get_zero_interface",
                          "[BignumInterface]", IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const BignumTy one = BignumTy::get_one();
  REQUIRE(one.is_valid());

  const BignumTy zero = BignumTy::get_zero();
  REQUIRE(zero.is_valid());
}

// =========================  comparison methods available
TEMPLATE_TEST_CASE_METHOD(Fixture, "comparison_interface", "[BignumInterface]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const BignumTy one = BignumTy::get_one();
  REQUIRE(one == one);
  REQUIRE(!(one < one));

  const BignumTy zero = BignumTy::get_zero();
  REQUIRE(zero == zero);
  REQUIRE(!(zero < zero));
}

// ========================= mix/max_element methods available
TEMPLATE_TEST_CASE_METHOD(Fixture, "min_max_interface", "[BignumInterface]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const BignumTy min_element = BignumTy::min();
  REQUIRE(min_element.is_valid());
  const BignumTy max_element = BignumTy::max();
  REQUIRE(max_element.is_valid());

  const bool call_ok =
      (min_element < max_element) || min_element == max_element;

  REQUIRE(call_ok);
}

// ========================= from_hexadecimal_str methods available
TEMPLATE_TEST_CASE_METHOD(Fixture, "from_hexadecimal_str_interface", "[BignumInterface]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;

  const BignumTy zero = BignumTy::get_zero();
  const std::optional<BignumTy> zero_from_str =
      BignumTy::from_hexadecimal_str("+0");
  REQUIRE(zero_from_str.has_value());
  REQUIRE(zero_from_str.value().is_valid());
  REQUIRE(zero == zero_from_str);

  const BignumTy one = BignumTy::get_one();
  const std::optional<BignumTy> one_from_str =
      BignumTy::from_hexadecimal_str("+1");
  REQUIRE(one_from_str.has_value());
  REQUIRE(one_from_str.value().is_valid());
  REQUIRE(one == one_from_str);
}

// ========================= to_hexadecimal_str methods available
TEMPLATE_TEST_CASE_METHOD(Fixture, "to_hexadecimal_str_interface", "[BignumInterface]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;
  constexpr auto buffer_size = 128;
  char buffer[buffer_size];

  const BignumTy one = BignumTy::get_one();
  const auto end = one.to_hexadecimal_str(buffer, buffer_size);
  REQUIRE(buffer != end);
}

// ========================= Mul method available.
TEMPLATE_TEST_CASE_METHOD(Fixture, "mul_interface", "[BignumInterface]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;
  const BignumTy one = BignumTy::get_one();
  const BignumTy zero = BignumTy::get_zero();
  REQUIRE(zero.mul(one) == zero);
}

// ========================= Sum method available
TEMPLATE_TEST_CASE_METHOD(Fixture, "sum_interface", "[BignumInterface]",
                          IMPLEMENTATIONS_TO_TEST) {
  using BignumTy = TestType;
  const BignumTy zero = BignumTy::get_zero();
  const BignumTy one = BignumTy::get_one();
  REQUIRE(zero.add(one) == one);
}
