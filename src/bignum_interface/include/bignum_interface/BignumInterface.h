#pragma once

#include <optional>
#include <string_view>

namespace bignum {

/// Class representing an integer type that:
/// 1. Is aware of its own precision.
/// 2. Is capable of detecting overflow.
/// 3. Is capable of converting to and from a hexadecimal representation.

/// The precision representable by this type depends on its
/// implementation and may be infinite (given infinite memory).
///
/// The method `is_valid` allows clients to query whether operations have
/// caused the number to overflow or underflow. Invalid objects are no longer
/// usable and should be destroyed or reassigned.
///
/// Constructors are defined by each implementation, since they may provide more
/// efficient interfaces or specialized methods to create Bignum objects with
/// high precision. Furthermore, limiting constructors through the type system
/// provide important constraints on how the class is used; for instance, a
/// Bignum type representing integers in the [-128, 127] interval would not
/// provide a constructors from `int32_t` or `int64_t`. A compiler configured
/// with good warnings would warn users on dangerous narrowing conversions.
///
/// All methods not expected to perform allocations are required to be
/// constexpr. Other methods can be made constexpr by implementations.
///
/// Implementations are not supposed to inherit from this class. The interface
/// is enforced through unit tests. This will be done with Concepts, once
/// support is available (c++20).
struct Bignum final {

  /// Returns a Bignum representing the product of `this` and `other`.
  /// The returned object will be invalid if and only if the operation overflows
  /// or underflow.
  /// Precondition: `this` and `other` are valid.
  [[nodiscard]] Bignum mul(const Bignum &other) const;

  /// Returns a Bignum representing the sum of `this` and `other`.
  /// The returned object will be invalid if and only if the operation overflows
  /// or underflows.
  /// Precondition: `this` and `other` are valid.
  [[nodiscard]] Bignum add(const Bignum &other) const;

  // c++20: spaceship operator.
  /// Returns true if and only if `this` < `other`.
  /// Precondition: `this` and `other` are valid.
  [[nodiscard]] constexpr bool operator<(const Bignum &other) const;

  /// Returns true if and only if `this` == `other`.
  /// Precondition: `this` and `other` are valid.
  [[nodiscard]] constexpr bool operator==(const Bignum &other) const;

  /// Returns a Bignum object representing the identity element for
  /// the addition operation.
  /// That is, for any valid Bignum object `x`:
  /// `x.add(get_zero()) == x` and `get_zero().add(x) == x`.
  [[nodiscard]] static Bignum get_zero();

  /// Returns a Bignum object representing the identity element for
  /// the multiplication operation.
  /// That is, for any valid Bignum object `x`:
  /// `x.mul(get_one()) == x` and `get_one().mul(x) == x`.
  [[nodiscard]] static Bignum get_one();

  /// Returns true if and only if the Bignum is able to represent an arbitrarily
  /// large or small integer given infinite memory.
  [[nodiscard]] static constexpr bool has_arbitrary_precision();

  /// Returns the smallest representable Bignum object.
  /// Only available if `Bignum::has_arbitrary_precision()` is false.
  template <typename T = Bignum,
            typename = enable_if_t<!T::has_arbitrary_precision()>>
  [[nodiscard]] static Bignum min();

  /// Returns the largest representable Bignum object.
  /// Only available if `Bignum::has_arbitrary_precision()` is false.
  template <typename T = Bignum,
            typename = enable_if_t<!T::has_arbitrary_precision()>>
  [[nodiscard]] static Bignum max();

  /// Returns true if and only if the object represents a valid number.
  /// The only allowed operations on an invalid object are:
  /// 1. Copy/Move assignment operators; and
  /// 2. `is_valid` calls; and
  /// 3. Destructor calls.
  template <typename T = Bignum,
            typename = enable_if_t<!T::has_arbitrary_precision()>>
  [[nodiscard]] bool is_valid() const;

  /// Bignum types for which `has_arbitrary_precision` returns true
  /// must return true for `is_valid`.
  template <typename T = Bignum,
            typename = enable_if_t<T::has_arbitrary_precision()>>
  [[nodiscard]] constexpr bool is_valid() const {
    return true;
  }

  /// Constructs a Bignum from a string of the format:
  ///                  (+|-)?[1-9a-f][0-9a-f]*
  /// The returned object, if any, is guaranteed to valid.
  /// An empty optional is returned if:
  /// 1. `str` is ill-formed.
  /// [For types with limited precision:]
  /// 2. The number represented by `str` is larger than `max()` or
  /// 3. The number represented by `str` is smaller than `min()`.
  [[nodiscard]] static std::optional<Bignum>
  from_hexadecimal_str(std::string_view str);

  /// Converts a Bignum to a string of the format:
  ///                  (+|-)[1-9][0-9]*
  /// The pair `begin, size` has the semantics of a span.
  /// If successful, returns a pointer one past the last character written.
  /// If errors occurs, returns begin.
  /// c++20: use span.
  [[nodiscard]] char *to_hexadecimal_str(char *begin, int64_t size) const;
};
} // namespace bignum
