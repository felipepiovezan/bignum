#pragma once

#include <limits>
#include <type_traits>
#include <utility>

namespace stl_extras {

/// Returns the absolute value of v.
/// This is useful, since `std::abs` is not constexpr. Furthermore, this method
/// forces users to acknowledge its unsafeness for a given input.
/// Precondition: If IntTy is a signed type then v !=
/// numeric_limits<IntTy>::min.
template <typename IntTy> constexpr IntTy unsafe_abs(IntTy v) {
  static_assert(std::is_integral_v<IntTy>);
  if constexpr (std::is_unsigned_v<IntTy>)
    return v;
  if (v < 0)
    return -v;
  return v;
}

/// Computes `X = v1 * v2 + carry` using DoubleValueTy and
/// returns a ValueTy pair containing:
/// - `X mod pow2` (The portion of X that does not overflow)
/// - `X / pow2`   (The portion of X that overflows, i.e. the carry.)
/// Where `pow2` is `1 + numeric_limits<ValueTy>::max`.
template <typename ValueTy, typename DoubleValueTy>
constexpr std::pair<ValueTy, ValueTy> mul_add_carry(ValueTy v1, ValueTy v2,
                                                    ValueTy carry) {

  constexpr auto pow2 =
      1 + static_cast<DoubleValueTy>(std::numeric_limits<ValueTy>::max());

  DoubleValueTy double_v1 = v1;
  DoubleValueTy double_v2 = v2;
  DoubleValueTy product = double_v1 * double_v2 + carry;
  auto modulus = static_cast<ValueTy>(product % pow2);
  auto quotient = static_cast<ValueTy>(product / pow2);

  return {modulus, quotient};
}

} // namespace stl_extras
