#pragma once

#include <cassert>
#include <fmt/format.h>
#include <limits>
#include <optional>
#include <string_view>

namespace bignum {

/// Bignum implementation using an integer and a bool.
///
/// All operations are performed on the integer and the boolean is
/// used to keep track of overflow. This is not an efficient class and is
/// meant as an example of how to implement the Bignum interface and how to
/// interface with the testing infrastructure.
///
/// Overflow detection is detected using GCC's and Clang's overflow builtins.
/// See: [GCC's
/// documentation](https://gcc.gnu.org/onlinedocs/gcc/Integer-Overflow-Builtins.html)
/// See: [Clang's
/// documentation](https://gcc.gnu.org/onlinedocs/gcc/Integer-Overflow-Builtins.html)
///
/// Constructors from unsigned types are deleted, since the resulting object
/// would not necessarily be valid.
///
/// The documentation for methods shared with the common interface is provided
/// there (Bignum).
class BignumInt {
  using ValueTy = int32_t;
  using UnsignedValueTy = uint32_t;

public:
  [[nodiscard]] static constexpr bool has_arbitrary_precision() {
    return false;
  }

  [[nodiscard]] static constexpr BignumInt get_zero() { return BignumInt(0); }
  [[nodiscard]] static constexpr BignumInt get_one() { return BignumInt(1); }

  [[nodiscard]] constexpr bool is_valid() const { return valid; }

  [[nodiscard]] constexpr bool operator<(const BignumInt &other) const {
    assert(is_valid());
    assert(other.is_valid());
    return val < other.val;
  }
  [[nodiscard]] constexpr bool operator==(const BignumInt &other) const {
    assert(is_valid());
    assert(other.is_valid());
    return val == other.val;
  }

  [[nodiscard]] constexpr static BignumInt min() {
    return BignumInt(std::numeric_limits<ValueTy>::min());
  }
  [[nodiscard]] constexpr static BignumInt max() {
    return BignumInt(std::numeric_limits<ValueTy>::max());
  }

  [[nodiscard]] static std::optional<BignumInt>
  from_hexadecimal_str(std::string_view str);

  [[nodiscard]] char *to_hexadecimal_str(char *begin, int64_t size) const;

  [[nodiscard]] constexpr BignumInt mul(const BignumInt &other) const {
    assert(is_valid() && other.is_valid());

    if (ValueTy res = 0; __builtin_mul_overflow(val, other.val, &res)) {
      auto invalid = get_zero();
      invalid.valid = false;
      return invalid;
    } else {
      return BignumInt(res);
    }
  }

  [[nodiscard]] constexpr BignumInt add(const BignumInt &other) const {
    assert(is_valid() && other.is_valid());
    if (ValueTy res = 0; __builtin_add_overflow(val, other.val, &res)) {
      auto invalid = get_zero();
      invalid.valid = false;
      return invalid;
    } else {
      return BignumInt(res);
    }
  }

  /// Constructs a BignumInt representing the same value as `val_`.
  /// The returned object is guaranteed to be valid.
  explicit constexpr BignumInt(ValueTy val_) : val{val_}, valid{true} {}

  BignumInt(UnsignedValueTy val_) = delete;

private:
  ValueTy val;
  bool valid;

  friend struct fmt::formatter<BignumInt>;
};
} // namespace bignum

/// Formatter for the binary representation of BignumInt.
/// The resulting string follows the pattern:
/// `valid = [01] value = [01]^32`
template <> struct fmt::formatter<bignum::BignumInt> {
  using BignumTy = bignum::BignumInt;

  template <typename ParseContext> constexpr auto parse(ParseContext &ctx) {
    return ctx.begin();
  }

  template <typename FormatContext>
  auto format(const BignumTy &to_print, FormatContext &ctx) {
    format_to(ctx.out(), "valid = {:b} value = {:0>32b}", to_print.valid,
              to_print.val);
    return ctx.out();
  }
};
