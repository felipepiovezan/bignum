#include "bignum_int/bignum_int.h"
#include "charconv_helper/charconv_helper.h"

#include <cassert>

using namespace bignum;

std::optional<BignumInt> BignumInt::from_hexadecimal_str(std::string_view str) {
  if (auto optional_value = charconv_helper::from_chars<ValueTy>(str))
    return BignumInt(optional_value.value());
  return {};
}

char *BignumInt::to_hexadecimal_str(char *begin, int64_t size) const {
  assert(is_valid());
  return charconv_helper::to_chars(begin, size, val);
}
