#include <charconv>
#include <optional>

namespace {
constexpr auto HEXADECIMAL = 16;
}

namespace charconv_helper {

  [[nodiscard]] inline bool has_minus_sign(std::string_view str) {
    return str.size() > 0 && str[0] == '-';
  }
  [[nodiscard]] inline bool has_plus_sign(std::string_view str) {
    return str.size() > 0 && str[0] == '+';
  }

  template <typename ValueTy>
  std::optional<ValueTy> from_chars(std::string_view str) {
    if (str.size() == 0)
      return {};

    // std::from_chars does not accept the + prefix.
    if (str[0] == '+') {
      str.remove_prefix(1);

      if (str.size() == 0)
        return {};
    }

    auto begin = str.data();
    auto end = begin + str.size();

    ValueTy as_value = 0;
    auto match_result = std::from_chars(begin, end, as_value, HEXADECIMAL);

    // `from_chars` indicates that:
    // - Failure if match_result.ptr == begin.
    // - Not all characters were used if `match_result.ptr != end`
    // This last condition may happen due to invalid pattern or if out of the
    // representable range.
    if (match_result.ptr != end)
      return {};

    return as_value;
  }

  template <typename ValueTy>
  char* to_chars(char *begin, int64_t size, const ValueTy& convert_me) {
    if (begin == nullptr || size <= 0)
      return begin;

    auto end = begin + size;
    auto result = std::to_chars(begin, end, convert_me, HEXADECIMAL);

    // From the `to_chars` documentation:
    // On success, returns a value of type to_chars_result such that ec equals
    // value-initialized std::errc
    if (result.ec != std::errc())
      return begin;
    return result.ptr;
  }
}
