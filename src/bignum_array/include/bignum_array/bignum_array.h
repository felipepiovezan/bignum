#pragma once

#include "stl_extras/arithmetic.h"
#include <fmt/format.h>

#include <algorithm>
#include <cassert>
#include <cmath>
#include <limits>
#include <optional>
#include <string_view>
#include <utility>

namespace bignum {

/// Bignum implementation using an array of unsigned integers, a sign bit and a
/// valid bit. The size of the array and, consequently, the precision can be
/// controlled through the `ARRAY_SIZE` template parameter.
///
/// Two's-complement is used for the internal representation and arithmetic
/// logic.
///
/// Overflow is detected using GCC's and Clang's overflow builtins.
/// See: [GCC's
/// documentation](https://gcc.gnu.org/onlinedocs/gcc/Integer-Overflow-Builtins.html)
/// See: [Clang's
/// documentation](https://gcc.gnu.org/onlinedocs/gcc/Integer-Overflow-Builtins.html)
///
/// A constructor from __int128_t is provided.
///
/// Clients should use one of the provided alias declarations, since the size of
/// BignumArray grows with `ARRAY_SIZE`. Furthermore, tests are only executed
/// for the provided alias declarations.
///
template <std::int64_t ARRAY_SIZE> class BignumArray {
  using ValueTy = uint32_t;
  using DoubleValueTy = uint64_t;
  using SignedValueTy = int32_t;
  using QuadSignedValueTy = __int128_t;
  using QuadValueTy = __uint128_t;

public:
  [[nodiscard]] static constexpr bool has_arbitrary_precision() {
    return false;
  }

  [[nodiscard]] static constexpr BignumArray get_zero() {
    return BignumArray(0);
  }
  [[nodiscard]] static constexpr BignumArray get_one() {
    return BignumArray(1);
  }

  [[nodiscard]] constexpr bool is_valid() const { return valid; }

  [[nodiscard]] constexpr bool operator<(const BignumArray &other) const {
    assert(is_valid());
    assert(other.is_valid());

    if (sign_bit && !other.sign_bit)
      return true;
    if (other.sign_bit && !sign_bit)
      return false;

    // c++20: constexpr.
    return std::lexicographical_compare(std::rbegin(vals), std::rend(vals),
                                        std::rbegin(other.vals),
                                        std::rend(other.vals));
  }

  [[nodiscard]] constexpr bool operator==(const BignumArray &other) const {
    assert(is_valid());
    assert(other.is_valid());

    if (sign_bit != other.sign_bit)
      return false;

    // c++20: constexpr.
    // return std::equal(std::begin(vals), std::end(vals),
    // std::begin(other.vals));
    for (int64_t it = 0, end = ARRAY_SIZE; it != end; ++it) {
      if (vals[it] != other.vals[it])
        return false;
    }

    return true;
  }

  [[nodiscard]] constexpr static BignumArray min() {
    BignumArray ans = get_zero();
    ans.sign_bit = true;
    return ans;
  }

  [[nodiscard]] constexpr static BignumArray max() {
    BignumArray ans = get_zero();

    for (auto &val : ans.vals)
      val = std::numeric_limits<ValueTy>::max();

    return ans;
  }

  [[nodiscard]] static std::optional<BignumArray>
  from_hexadecimal_str(std::string_view str);

  [[nodiscard]] char *to_hexadecimal_str(char *begin, int64_t size) const;

  /// Multiplication is implemented using the "pen-and-paper algorithm".
  /// For example, if:
  /// `a = abs(this)`,  `a_i` the i-th "digit" of `a`
  /// `b = abs(this)`,  `b_i` the i-th "digit" of `b`
  /// Where the digits are in base `(1 + numeric_limits::max<ValueTy>)`.
  /// Then:
  ///   a2                        a1                        a0
  /// x b2                        b1                        b0
  /// -----------------------------------------------------------------
  ///   (a2*b0 + carry2)%base     (a1*b0 + carry1)%base     (a0*b0)%base
  /// ... repeat for b1, shift values left by one
  /// ... repeat for b2, shift values left by two
  /// ... add all the results.
  /// Adjust sign based on `this` and `other`.
  [[nodiscard]] constexpr BignumArray mul(const BignumArray &other) const {
    assert(is_valid() && other.is_valid());

    const BignumArray v1 = unsafe_abs();
    const BignumArray v2 = other.unsafe_abs();

    // If either operand was == min, then unsafe_abs returns a negative number.
    // In this case, mul is only valid if the other operand is zero.
    if (v1.is_negative() && v2 == get_zero())
      return get_zero();
    if (v2.is_negative() && v1 == get_zero())
      return get_zero();
    if (v1.is_negative() || v2.is_negative())
      return get_invalid();

    BignumArray ans = non_negative_mul(v1, v2);

    if (ans.is_valid() && ((is_negative() ^ other.is_negative())))
      ans.twos_complement();

    return ans;
  }

  [[nodiscard]] constexpr BignumArray add(const BignumArray &other) const {
    assert(is_valid() && other.is_valid());

    auto ans = unsafe_add(*this, other);

    if (!ans.is_negative() && is_negative() && other.is_negative())
      ans.valid = false;
    if (ans.is_negative() && !is_negative() && !other.is_negative())
      ans.valid = false;

    return ans;
  }

  /// Constructs a BignumInt representing the same value as `val`.
  /// The returned object is guaranteed to be valid.
  explicit constexpr BignumArray(const SignedValueTy val)
      : vals{0}, valid{true}, sign_bit{0} {

    // If val = numeric_limits::min, we can't represent its addition inverse
    // using two's complement: it would overflow.
    bool const is_min_representable =
        val == std::numeric_limits<SignedValueTy>::min();
    vals[0] = static_cast<ValueTy>(
        stl_extras::unsafe_abs(val + is_min_representable));

    if (val < 0)
      twos_complement();

    // The input LSB is 0 in the numeric_limits::min case.
    // Unset the LSB to revert the "+1" done previously.
    vals[0] &= ~static_cast<ValueTy>(is_min_representable);
  }

  /// Constructs a BignumInt representing the same value as `quadval`.
  /// The returned object is guaranteed to be valid.
  explicit constexpr BignumArray(QuadSignedValueTy quadval)
      : vals{0}, valid{true}, sign_bit{0} {

    // If quadval = numeric_limits::min, we can't represent
    // its addition inverse using two's complement: it would overflow.
    bool is_min_representable =
        quadval == std::numeric_limits<QuadSignedValueTy>::min();
    auto u_quadval = static_cast<QuadValueTy>(
        stl_extras::unsafe_abs(quadval + is_min_representable));

    for (auto &val : vals) {
      val = static_cast<ValueTy>(u_quadval);
      u_quadval >>= (8 * sizeof(ValueTy));
    }

    if (quadval < 0)
      twos_complement();

    // The input LSB is 0 in the numeric_limits::min case.
    // Unset the LSB to revert the "+1" done previously.
    vals[0] &= ~static_cast<ValueTy>(is_min_representable);
  }

  BignumArray(ValueTy val_) = delete;
  BignumArray(QuadValueTy quadval) = delete;

  /// Returns true if and only if `this < get_zero()` is true.
  [[nodiscard]] constexpr bool is_negative() const { return sign_bit; }

private:
  ValueTy vals[ARRAY_SIZE];
  ValueTy valid : 1;
  ValueTy sign_bit : 1;

  constexpr static BignumArray get_invalid() {
    auto ans = get_zero();
    ans.valid = 0;
    return ans;
  }
  /// Converts `this` into its two's complement representation, effectively
  /// transforming `this` into its addition inverse.
  constexpr void twos_complement() {
    bool found_one = false;

    for (auto &val : vals) {
      if (found_one) {
        val = ~val;
        continue;
      }

      found_one |= val;
      val = (~val) + 1;
    }

    if (found_one)
      sign_bit ^= 1;
  }

  /// Returns the absolute value of `this`.
  /// If `this == min()`, returns min().
  [[nodiscard]] constexpr BignumArray unsafe_abs() const {
    if (is_negative()) {
      BignumArray addition_inverse = *this;
      addition_inverse.twos_complement();
      return addition_inverse;
    }
    return *this;
  }

  /// Returns v1 * v2.
  /// Precondition: `v1 > 0` and `v2 > 0`.
  static constexpr BignumArray non_negative_mul(const BignumArray &v1,
                                                const BignumArray &v2) {
    BignumArray ans = get_zero();
    int64_t idx = 0;

    for (auto val : v1.vals) {
      if (val == 0) {
        idx++;
        continue;
      }

      BignumArray partial = get_zero();
      ValueTy carry = 0;
      int64_t other_idx = idx;

      for (auto other_val : v2.vals) {
        auto [product, new_carry] =
            stl_extras::mul_add_carry<ValueTy, DoubleValueTy>(val, other_val,
                                                              carry);

        if (other_idx < ARRAY_SIZE)
          partial.vals[other_idx] = product;
        else if (product != 0) {
          ans.valid = false;
          break;
        }

        carry = new_carry;
        other_idx++;
      }

      if (carry)
        ans.valid = false;
      if (ans.valid == false)
        break;

      ans = ans.add(partial);
      idx++;
    }

    return ans;
  }

  /// Returns v1 + v2. The result is set to valid regardless of overflow.
  static constexpr BignumArray unsafe_add(const BignumArray &v1,
                                          const BignumArray &v2) {
    BignumArray ans = get_zero();
    bool previous_carry = false;

    // c++20: ranges.
    for (int64_t it = 0, end = ARRAY_SIZE; it != end; ++it) {
      // Note: at most one of these operations will overflow.
      bool carry =
          __builtin_add_overflow(v1.vals[it], v2.vals[it], &ans.vals[it]);
      carry |=
          __builtin_add_overflow(ans.vals[it], previous_carry, &ans.vals[it]);
      previous_carry = carry;
    }

    bool const sum_is_negative =
        v1.is_negative() ^ v2.is_negative() ^ previous_carry;
    ans.sign_bit = sum_is_negative;

    return ans;
  }

  friend struct fmt::formatter<BignumArray>;
};
} // namespace bignum

/// Formatter for the binary representation of BignumArray.
/// The resulting string follows the pattern:
/// `valid = [01] value = ([01]^32)^ARRAY_SIZE`
template <std::int64_t ARRAY_SIZE>
struct fmt::formatter<bignum::BignumArray<ARRAY_SIZE>> {
  using BignumTy = bignum::BignumArray<ARRAY_SIZE>;

  template <typename ParseContext> constexpr auto parse(ParseContext &ctx) {
    return ctx.begin();
  }

  template <typename FormatContext>
  auto format(const BignumTy &to_print, FormatContext &ctx) {
    format_to(ctx.out(), "Valid = {},  Sign = {}, Values = ", to_print.valid,
              to_print.sign_bit);

    // c++20 : ranges.
    for (auto it = std::rbegin(to_print.vals), end = std::rend(to_print.vals);
         it != end; ++it)
      format_to(ctx.out(), "{:0>32b},", *it);
    return ctx.out();
  }
};
