#include "bignum_array/bignum_array.h"
#include "charconv_helper/charconv_helper.h"

#include <cassert>
#include <utility>

using namespace bignum;

namespace {

constexpr int64_t hex_digits_per_byte = 8;

template <typename ValueTy>
constexpr int64_t hex_digits_per_idx = hex_digits_per_byte * sizeof(ValueTy);

/// Consumes characters from the end of `str` to create a `ValueTy` object. The
/// `str` object is expected to represent a hexadecimal value with no leading
/// `-` or `+` sign. Returns a pair containing:
///  1. The prefix of `str` not used by this function.
///  2. An optional containing the created `ValueTy` object, if successful.
template <typename ValueTy>
std::pair<std::string_view, std::optional<ValueTy>>
convert_one_word(std::string_view str) {

  int64_t start = [str]() {
    if (str.size() < hex_digits_per_idx<ValueTy>)
      return int64_t(0);
    return static_cast<int64_t>(str.size()) - hex_digits_per_idx<ValueTy>;
  }();

  auto substr =
      str.substr(static_cast<std::size_t>(start),
                 static_cast<std::size_t>(hex_digits_per_idx<ValueTy>));
  auto leftover_str = str.substr(0, static_cast<std::size_t>(start));

  return {leftover_str, charconv_helper::from_chars<ValueTy>(substr)};
}

/// Returns a view from `str` with all leading zeros removed, or an empty view
/// if no leading zeros exist.
std::string_view drop_leading_zeros_or_empty(std::string_view str) {
  auto first_non_zero = str.find_first_not_of('0');

  if (first_non_zero == std::string_view::npos)
    return std::string_view();

  return str.substr(first_non_zero, str.size());
}
}

template <int64_t ARRAY_SIZE>
std::optional<BignumArray<ARRAY_SIZE>>
BignumArray<ARRAY_SIZE>::from_hexadecimal_str(std::string_view str) {
  bool is_negative = false;

  if (charconv_helper::has_minus_sign(str)) {
    str.remove_prefix(1);
    is_negative = true;
  }

  // Handle "-", "+", "" inputs.
  if (str.empty())
    return{};

  BignumArray answer = get_zero();

  for (auto &val : answer.vals) {
    auto [leftover_str, optional_value] = convert_one_word<ValueTy>(str);

    if(!optional_value.has_value())
      return {};

    val = optional_value.value();
    str = leftover_str;

    if (str.empty())
      break;
  }

  if (drop_leading_zeros_or_empty(str).empty() == false)
    return {};

  if (is_negative)
    answer.twos_complement();

  return answer;
}

template <int64_t ARRAY_SIZE>
char *BignumArray<ARRAY_SIZE>::to_hexadecimal_str(char *begin,
                                                  int64_t size) const {

  auto *failure = begin;

  if (begin == nullptr)
    return failure;
  if (size < hex_digits_per_idx<ValueTy> * int64_t(ARRAY_SIZE))
    return failure;

  for (const auto &val : vals) {

    auto *new_begin =
        charconv_helper::to_chars(begin, hex_digits_per_idx<ValueTy>, val);

    if (new_begin == begin)
      return failure;

    begin = new_begin;
  }

  return begin;
}

// Provide some instantiations of BignumArray to prevent leaking <charconvs>
// into the interface.
template class bignum::BignumArray<4>;
