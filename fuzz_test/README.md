# Adding a new fuzzzer

A fuzzer consists of exactly one function, which should be as simple as
possible. Since each different Bignum test must be tested individually, the
actual implementation of fuzzers are placed in individual headers.

When adding a new Bignum implementation, define the needed tests inside `.cpp`
files and add a new target in `CMakeLists.txt`:

```cmake
add_fuzz_test(NAME         bignum_<implementation name>_fuzzer_<fuzzer name>
              SOURCES      <cpp file>
              LIBS_TO_LINK <implementation library>)
```

Important: make sure to mark the function calling the Bignum function as
`[[clang::optnone]]`. Alternatively, check the code coverage reports to ensure
your testing is adequate.

For a simple example, check `fuzzer_from_str.h` and
`bignum_int_fuzzer_from_str.cpp`.
