#include "bignum_int/bignum_int.h"
#include "fuzzer_ctor_int.h"

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *data, size_t data_size) {
  sum_values<bignum::BignumInt, int>(data, data_size);
  return 0;
}
