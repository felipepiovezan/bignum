#pragma once

#include <cassert>

namespace {
/// Utility function for fuzzers testing the constructors taking
/// an integer type. Uses `data` to build an `IntTy` and calls
/// the Bignum ctor.
template <typename BignumTy, typename IntTy>
[[clang::optnone]] void sum_values(const uint8_t *data, size_t data_size) {

  constexpr auto bytes_per_int = static_cast<int64_t>(sizeof(IntTy));
  IntTy fuzz_int(0);

  auto end = std::min(bytes_per_int, static_cast<int64_t>(data_size));
  for (int64_t offset = 0; offset != end; ++offset) {
    reinterpret_cast<uint8_t *>(&fuzz_int)[offset] = data[offset];
  }

  static_cast<void>(BignumTy(fuzz_int));
}
} // namespace
