#pragma once

#include <utility>

namespace {
template <typename BignumTy>
/// Utility function for fuzzers testing the `from_str` method of Bignum
/// implementations. Uses `data` to call `from_hexadecimal_str`.
[[clang::optnone]] void sum_values_from_str(const char *data,
                                            size_t data_size) {
  auto view = std::string_view(data, data_size);
  static_cast<void>(BignumTy::from_hexadecimal_str(view));
}
} // namespace
