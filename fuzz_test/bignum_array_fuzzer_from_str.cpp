#include "bignum_array/bignum_array.h"
#include "fuzzer_from_str.h"

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *data, size_t data_size) {
  sum_values_from_str<bignum::BignumArray<4>>(reinterpret_cast<const char *>(data), data_size);
  return 0;
}
