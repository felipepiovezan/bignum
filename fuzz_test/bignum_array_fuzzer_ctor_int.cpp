#include "bignum_array/bignum_array.h"
#include "fuzzer_ctor_int.h"

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *data, size_t data_size) {
  sum_values<bignum::BignumArray<4>, __int128_t>(data, data_size);
  return 0;
}
