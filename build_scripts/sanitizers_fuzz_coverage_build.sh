usage="sanitizers_build <path_to_src> <path_to_build>"

if [ "$#" -ne 2 ]; then
  echo "Usage: $usage"
  exit 1
fi

set -x

source_dir="$1"
build_dir="$2"
generator="-G Ninja"
sanitizer_options="-DENABLE_COVERAGE=ON -DENABLE_SANITIZER_ADDRESS=ON -DENABLE_SANITIZER_MEMORY=OFF -DENABLE_SANITIZER_UNDEFINED_BEHAVIOR=ON"
compiler="-DCMAKE_CXX_COMPILER=clang++"
build_kind="-DCMAKE_BUILD_TYPE=RelWithDebInfo"

cmake -S $source_dir \
      -B $build_dir \
      $generator \
      $sanitizer_options \
      $build_kind \
      $compiler

cmake --build $build_dir --target all_fuzzers run_unittests
