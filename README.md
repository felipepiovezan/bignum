# Table of Contents

1. [This project](#this-project)
2. [The path](#the-path)
3. [The interface](#the-interface)
    1. [Enforcing the interface](#enforcing-the-interface)
    2. [Enforcing the interface: Concepts](#enforcing-the-interface-concepts)
    3. [Enforcing the interface: CRTP](#enforcing-the-interface-crtp)
    4. [Enforcing the interface: Unit tests](#enforcing-the-interface-unit-tests)
4. [Benchmark](#benchmark)
5. [Building the project](#building-the-project)
6. [Code coverage and sanitizers](#code-coverage-and-sanitizers)

# This project

This is my attempt at defining a simple class representing an integer
with large - potentially infinite - precision.

My main goals are:

1. Develop a library following the path and design decisions outlined below.
2. Try out modern C++ libraries/tools: [fmt], [Conan], [Catch2], [Clang's
libFuzzer].
4. Use C++20 facilities as soon as they are available (look for "c++20" tags along
the project).
3. Investigate execution time bottlenecks.

[fmt]: https://github.com/fmtlib/fmt
[Conan]: https://conan.io/
[Catch2]: https://github.com/catchorg/Catch2
[Clang's libFuzzer]: http://llvm.org/docs/LibFuzzer.html

# The path

1. Understand what is the minimal interface that I need to represent everything
I want out of a "big int" class.
2. Understand how can I force implementations to follow this interface
efficiently.
3. Testing (unit __and fuzzing__) new implementations should be trivial.
4. Implement simple versions of this class and benchmark them against each
other. This could be as simple as the "pen and paper" multiplication algorithm
using `std::array`.
5. Implement more complex versions of this class using, for instance, fast
Fourier transform algorithms in a discrete setting.
6. GPU implementation, using FFTs over integer rings (a better version of [this])!

[this]: https://gitlab.com/felipepiovezan/cuda_bignum

# The interface

A big integer class should know whether it provides infinite precision or not,
and this should be compile-time testable. For classes with limited precision,
the class should know the range of valid integers it can represent, in the style
of `std::min` and `std::max`.

Any integer type should provide addition and multiplication operations and, for
types with limited precision, it should be possible to test for overflow or
underflow.

An implementation must provide methods returning "one" (multiplication
identity) and "zero" (addition identity).

The type should provide conversions to and from strings representing
hexadecimal numbers. This is merely for convenience and for testing purposes,
otherwise clients would need to "start" from "1" and generate other numbers
through additions and multiplications.

This interface is described in
`bignum/src/bignum_interface/include/bignum_interface/BignumInterface.h`.

There are still undecided points, like whether implementations should provide
an `fmt` friendly formatter. Check the issues list for such discussions.

# Enforcing the interface

Requirements:

* Minimal, if any, runtime overhead incurred by following the interface.
* Different `Bignum` implementations are not expected to work with each other.

In practice, there is no need for implementations to derive from an abstract
base class defining the interface or use virtual methods.

That said, I insist that an implementation not following the interface should
NOT compile; this makes it a lot easier to use, test and benchmark
implementations of those types.

There are a few possible strategies to enforce the interface:

## Enforcing the interface: Concepts

This is exactly what we need: we want to describe what a Bignum concept is.
However, [Clang does not implement] all of concepts and [neither does GCC].

[Clang does not implement]: https://clang.llvm.org/cxx_status.html
[neither does GCC]: https://gcc.gnu.org/projects/cxx-status.html

In a future patch, I will attempt to study and learn more about concepts and
write what it should look like in C++20, even if it's not yet possible to
compile this.

## Enforcing the interface: CRTP

If we use CRTP, the base class can statically assert that the derived class
has all the properties we desire.

Pros:
1. Compile errors are clean.
2. Errors happen when the class is instantiated.

Cons:
1. Implementations need to follow the CRTP idiom for this to work.
2. A base class is used, this puts some strain on the optimizer to do the right
thing.
3. Compile time might increase as a result of more inlining and more template
magic.

## Enforcing the interface: Unit tests

This is the currently adopted strategy:

* Write generic unit tests for all methods of the interface.
* Implementations have to register themselves in `test/test_support.h`.
* Non-conforming implementations will fail at test compile time.

This is not nearly as good as concepts, but will accomplish the goals while we
wait for C++20 implementations to ship! Because implementations don't have to
use base classes or any other abstractions, there is no overhead in following
the interface.

# Benchmark

This is still a work in progress :)

The current plan is to use the google-bench library for this.

# Building the project

You'll need:

* A **recent** version of Clang (GCC is OK if you disable fuzzing and code
coverage). This project will start using  C++20 features soon, so a
tip-of-trunk compiler will be required soon.
* A new version of CMake, capable of downloading Conan.

To build:

```sh
cmake -S <source_directory> -B <build_directory> -DCMAKE_CXX_COMPILER=clang++
cmake --build <build_directory>
```

To run the tests:

```sh
cmake --build <build_directory> --target all_fuzzers run_unittests
```

## Code coverage and sanitizers

Check the recommended [build
script](build_scripts/sanitizers_fuzz_coverage_build.sh) for developers, which
enables sanitizers and code coverage.

Code coverage reports by default are placed in `<build_directory>/coverage_reports`,
this can be overriden with the CMake option `COVERAGE_REPORTS_LOCATION`.
